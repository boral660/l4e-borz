void Window::startGame()
{
    m_game = new Game(ui.glWidget->renderer());
    if (!m_game->trySetup())
    {
        QMessage:critical(NULL, "Unable to start game", "Failed to launch the game");
        start();
    }
    else
    {
        m_game->run();
    }