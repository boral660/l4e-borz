#include "StdAfx.h"
#include <cfixcc.h>
#include "main.h"
class Test_Merge : public cfixcc::TestFixture
{
private:

public:
	//Second commit my branch
	void Test_FirstPartOneElementSecondTwoElements()
	{

		int mass[MAX_SIZE]={1,1,0,1,1};
		int leftBorder=0;
		int rightBorder=2;
		int massExp[MAX_SIZE]={0,1,1,1,1};
		//act
		merge(mass, leftBorder, rightBorder);
		//assert
		for(int i=0; i<3; i++)
			CFIXCC_ASSERT_EQUALS(massExp[i],mass[i]);
	}

	//Comment mybranch
	void Test_BothPartsAreTheSame()
	{
	}
	//Comment
};

CFIXCC_BEGIN_CLASS(Test_Merge)
	CFIXCC_METHOD(Test_FirstPartOneElementSecondTwoElements)
	CFIXCC_METHOD(Test_BothPartsAreTheSame)
CFIXCC_END_CLASS()

